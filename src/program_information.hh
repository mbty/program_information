// This file is part of the program_information library -
// gitlab.com/mbty/program_information
#ifndef PROGRAM__INFORMATION_PROGRAM__INFROMATION_HH
#define PROGRAM__INFORMATION_PROGRAM__INFROMATION_HH
#include <string>
#include "../externals/version/src/version.hh"
namespace program_information {
  struct program_information {
    std::string      name;
    std::string      description;
    version::version version_number;
    std::string      version_name;
  };
}
#endif
