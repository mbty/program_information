# Program information
A simple C++ program information struct to hold meta information about a
program.

## Content
```C++
struct program_information {
  std::string      name;
  std::string      description;
  version::version version_number;
  std::string      version_name;
};
```

This struct is defined in the `program_information` namespace.

## Dependencies
This library depends on my [version](https://gitlab.com/mbty/version) library
for the version type.

## Legal
This library is free software. It comes without any warranty, to the extent
permitted by applicable law. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2, included
in the LICENSE file.

## Future
I consider this library to be complete.
